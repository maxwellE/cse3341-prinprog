import unittest
from Tokenizer import *


class TestTokenizerComprehensive(unittest.TestCase):
    def testgrabTokens(self):
        t = Tokenizer("""program begin end int if then else while
                      loop read write
                      ;,=![]&&||()+-*!===<><=>=
                      ; , = ! [ ] && || ( ) + - * != == < > <= >=
                      9014810948 2940830498329048 93048324983409
                      ASDSD1333 SADASDSD2333 SADAD""")
        token_list = []
        while (not t.on_final_token()):
            token_list.append(t.get_next_token_value())
            t.skip_token()
        self.assertEqual(range(1, 31) + range(12, 31) +
            [31, 31, 31, 32, 32, 32, 33], token_list)

    def testRepeatedEquality(self):
        t = Tokenizer("===XX")
        token_list = []
        while (not t.on_final_token()):
            token_list.append(t.get_next_token_value())
            t.skip_token()
        self.assertEqual([26, 14, 32, 33], token_list)

    def testMultiline(self):
        t2 = Tokenizer("""program
                          X
                          SSSA""")
        token_list = []
        while (not t2.on_final_token()):
            token_list.append(t2.get_next_token_value())
            t2.skip_token()
        self.assertEqual([1, 32, 32, 33], token_list)

    def testInvalidTokens(self):
        try:
            Tokenizer("This is invalid")
            self.fail()
        except SyntaxError:
            pass

    def testInvalidTokens2(self):
        try:
            Tokenizer("program ==dog")
            self.fail()
        except SyntaxError:
            pass

    def testInvalidTokens3(self):
        try:
            Tokenizer("dog==")
            self.fail()
        except SyntaxError:
            pass

    def testInvalidTokens4(self):
        try:
            Tokenizer("program 1T")
            self.fail()
        except SyntaxError:
            pass

    def testValidSmallToken(self):
        t = Tokenizer("program <=XXY<==4444==begin")
        token_list = []
        while (not t.on_final_token()):
            token_list.append(t.get_next_token_value())
            t.skip_token()
        self.assertEqual([1, 29, 32, 29, 14, 31, 26, 2, 33], token_list)

    def testAssertWord(self):
        t = Tokenizer("program <=XXY<==4444==begin")
        try:
            t.assert_next_token_word('progral')
            self.fail()
        except SyntaxError:
            pass

    def testAssertValue(self):
        t = Tokenizer("program <=XXY<==4444==begin")
        try:
            t.assert_next_token_value(20)
            self.fail()
        except SyntaxError:
            pass


if __name__ == "__main__":
    unittest.main()
