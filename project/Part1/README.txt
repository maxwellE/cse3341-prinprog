Interpreter Part 1
Maxwell Elliott: elliott.432@buckeyemail.osu.edu
====================================================

Files Included:
----------------------

Tokenizer.py - Main class for the Tokenizer and its functionality
Tokenizer.pyc - compiled python file, ignore
TokenizerTest.py - An automated PyUnit test suite made to test the 
Tokenizer,  to run just execute "$ python TokenizerTest.py".
test.core - a sample input file to the script.  the .core extension
has no affect other than looking nice.

To run the program:
---------------------------

$ ./Tokenizer.py <YOUR_FILE>

If there is an invalid token in your input file the program will throw
a SyntaxError and exit immedietly.

See DOCUMENTATION.TXT for more information.
