#! /usr/bin/env python
import Tokenizer
import Program
import sys
import Input


def main():
    token_file = open(sys.argv[1])
    Tokenizer.Tokenizer(token_file.read())
    if sys.argv[2]:
        input_file = open(sys.argv[2])
        Input.Input.load_input_list(input_file.read().strip())
    p = Program.Program()
    p.parse()
    print "\nPretty Printed version of Core program: \"%s\"" % sys.argv[1]
    print "============================================================"
    p.print_program()
    print "\nProgram Output"
    print "============================================================"
    p.execute()

if __name__ == "__main__":
    main()
