#!/usr/bin/env python
# encoding: utf-8
"""
Input.py

Created by Maxwell Elliott on 2012-10-23.
Copyright (c) 2012 . All rights reserved.
"""
import re
import Tokenizer
import IdList


class Input:
    input_list = []
    list_index = 0
    """docstring for Input"""
    def __init__(self):
        self.idlist = None

    def parse(self):
        Tokenizer.Tokenizer().assert_next_token_word("read")
        Tokenizer.Tokenizer().skip_token()
        self.idlist = IdList.IdList()
        self.idlist.parse()
        Tokenizer.Tokenizer().assert_next_token_word(";")
        Tokenizer.Tokenizer().skip_token()

    def print_input(self):
        print "read ",
        self.idlist.print_id_list()
        print ";"

    def execute(self):
        self.idlist.execute_input()

    @staticmethod
    def has_value():
        return Input.list_index < len(Input.input_list)

    @staticmethod
    def get_next_input_value():
        if Input.has_value():
            val = int(Input.input_list[Input.list_index])
            Input.list_index = Input.list_index + 1
            return val

    @staticmethod
    def load_input_list(source):
        Input.input_list = re.split('\s+', source)
