import Tokenizer
import Operand
import sys


class Comparison:
    def __init__(self):
        self.alternative = None
        self.op1 = None
        self.op2 = None

    def parse(self):
        Tokenizer.Tokenizer().assert_next_token_word("(")
        Tokenizer.Tokenizer().skip_token()
        self.op1 = Operand.Operand()
        self.op1.parse()
        if Tokenizer.Tokenizer().get_next_token_word() == "!=":
            self.alternative = 1
        elif Tokenizer.Tokenizer().get_next_token_word() == "==":
            self.alternative = 2
        elif Tokenizer.Tokenizer().get_next_token_word() == "<":
            self.alternative = 3
        elif Tokenizer.Tokenizer().get_next_token_word() == ">":
            self.alternative = 4
        elif Tokenizer.Tokenizer().get_next_token_word() == "<=":
            self.alternative = 5
        elif Tokenizer.Tokenizer().get_next_token_word() == ">=":
            self.alternative = 6
        else:
            raise SyntaxError("ERROR: Illegal token \"%s\" discovered, exiting now."
            % (Tokenizer.Tokenizer().get_next_token_word()))
            sys.exit(1)
        Tokenizer.Tokenizer().skip_token()
        self.op2 = Operand.Operand()
        self.op2.parse()
        Tokenizer.Tokenizer().assert_next_token_word(")")
        Tokenizer.Tokenizer().skip_token()

    def print_comp(self):
        print "(",
        self.op1.print_op()
        if self.alternative == 1:
            print " != ",
        elif self.alternative == 2:
            print " == ",
        elif self.alternative == 3:
            print " < ",
        elif self.alternative == 4:
            print " > ",
        elif self.alternative == 5:
            print " <= ",
        elif self.alternative == 6:
            print " >= ",
        self.op2.print_op()
        print ")",

    def evaluate_comparison(self):
        if self.alternative == 1:
            return self.op1.execute() != self.op2.execute()
        elif self.alternative == 2:
            return self.op1.execute() == self.op2.execute()
        elif self.alternative == 3:
            return self.op1.execute() < self.op2.execute()
        elif self.alternative == 4:
            return self.op1.execute() > self.op2.execute()
        elif self.alternative == 5:
            return self.op1.execute() <= self.op2.execute()
        elif self.alternative == 6:
            return self.op1.execute() >= self.op2.execute()
