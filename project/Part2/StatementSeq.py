#!/usr/bin/env python
# encoding: utf-8
"""
StatementSeq.py

Created by Maxwell Elliott on 2012-10-20.
"""
import Tokenizer
import Statement


class StatementSeq:
    """docstring for StatementSeq"""
    def __init__(self):
        self.statement = None
        self.statement_seq = None

    def print_ss(self):
        self.statement.print_stmnt()
        if self.statement_seq:
            self.statement_seq.print_ss()

    def parse(self):
        self.statement = Statement.Statement()
        self.statement.parse()
        if Tokenizer.Tokenizer().get_next_token_word() != "else" and Tokenizer.Tokenizer().get_next_token_word() != "end":
            self.statement_seq = StatementSeq()
            self.statement_seq.parse()

    def execute(self):
        self.statement.execute()
        if self.statement_seq:
            self.statement_seq.execute()
