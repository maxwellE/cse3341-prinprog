import Tokenizer
import DeclarationSeq
import StatementSeq


class Program:
    def __init__(self):
        self.ds = None
        self.ss = None

    def parse(self):
        Tokenizer.Tokenizer().assert_next_token_word('program')
        Tokenizer.Tokenizer().skip_token()
        self.ds = DeclarationSeq.DeclarationSeq()
        self.ds.parse()
        Tokenizer.Tokenizer().assert_next_token_word('begin')
        Tokenizer.Tokenizer().skip_token()
        self.ss = StatementSeq.StatementSeq()
        self.ss.parse()
        Tokenizer.Tokenizer().assert_next_token_word('end')
        Tokenizer.Tokenizer().skip_token()
        Tokenizer.Tokenizer().assert_next_token_value(33)
        Tokenizer.Tokenizer().skip_token()

    def print_program(self):
        print 'program'
        self.ds.print_ds()
        print 'begin'
        self.ss.print_ss()
        print 'end'

    def execute(self):
        self.ds.execute()
        self.ss.execute()
