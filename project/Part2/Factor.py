import Tokenizer
import Operand


class Factor:
    def __init__(self):
        self.operand = None
        self.factor = None

    def parse(self):
        self.operand = Operand.Operand()
        self.operand.parse()
        if Tokenizer.Tokenizer().get_next_token_word() == "*":
            self.factor = Factor.Factor()
            self.factor.parse()

    def print_fac(self):
        self.operand.print_op()
        if self.factor:
            print " * "
            self.factor.print_fac()

    def execute(self):
        if self.factor:
            return self.operand.execute() * self.factor.execute()
        else:
            return self.operand.execute()
