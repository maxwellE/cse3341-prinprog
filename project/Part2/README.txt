README.txt

Maxwell Elliott
CSE 3341 
Interpreter Project Part 2
elliott.432@osu.edu
============================================================================

File Manifest:
-----------------------------------
project/Part2/Assign.pyc
project/Part2/Comparison.pyc
project/Part2/Condition.pyc
project/Part2/Declaration.pyc
project/Part2/DeclarationSeq.pyc
project/Part2/Expression.pyc
project/Part2/Factor.pyc
project/Part2/Id.py
project/Part2/Id.pyc
project/Part2/IdList.pyc
project/Part2/If.py
project/Part2/If.pyc
project/Part2/Input.pyc
project/Part2/Loop.pyc
project/Part2/Operand.pyc
project/Part2/Output.pyc
project/Part2/Program.py
project/Part2/Program.pyc
project/Part2/Statement.py
project/Part2/Statement.pyc
project/Part2/StatementSeq.py
project/Part2/StatementSeq.pyc
project/Part2/Tab.py
project/Part2/Tab.pyc
project/Part2/Tokenizer.pyc
project/Part2/second2.txt
project/Part2/second3.txt
project/Part2/third.core
project/Part2/third1.txt
project/Part2/third2.txt
project/Part2/third3.txt

Ignore the pyc files, they are just a Python special thing.  Each of these files
is very straightforward, they all have a class with methods print, execute
and parse as defined in the recursive descent style.  Id is the only class that
is different as it requires additional functionality.

The core files are the provided test programs. The other txt files are the 
input for the Core test programs.

How to execute Interpreter
-----------------------------------

$ ./Interpreter.py <CORE_PROGRAM_FILE> <INPUT_FILE>

Thats it.  If you do not provide one of the two files the Interpreter will throw an error.
