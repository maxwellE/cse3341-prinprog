#!/usr/bin/env python
# encoding: utf-8
import IdList
import Tokenizer
"""
Declaration.py

Created by Maxwell Elliott on 2012-10-17.
"""


class Declaration:
    """docstring for Declaration"""
    def __init__(self):
        self.id_list = None

    def parse(self):
        """docstring for parse"""
        Tokenizer.Tokenizer().assert_next_token_word("int")
        Tokenizer.Tokenizer().skip_token()
        self.id_list = IdList.IdList()
        self.id_list.parse()
        Tokenizer.Tokenizer().assert_next_token_word(";")
        Tokenizer.Tokenizer().skip_token()

    def print_decl(self):
        """docstring for print_decl"""
        print "\tint ",
        self.id_list.print_id_list()
        print ";"

    def execute(self):
        self.id_list.execute_decl()
