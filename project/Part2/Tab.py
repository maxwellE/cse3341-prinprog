class Tab:
    num_tabs = 0

    @staticmethod
    def increase_tab():
        Tab.num_tabs += 1

    @staticmethod
    def decrease_tab():
        Tab.num_tabs -= 1

    @staticmethod
    def print_tabs():
        print "\t" * Tab.num_tabs,
