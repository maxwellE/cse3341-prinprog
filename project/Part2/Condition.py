import Tokenizer
import Comparison
import sys


class Condition:
    def __init__(self):
        self.comp = None
        self.cond1 = None
        self.cond2 = None
        self.alternative = None

    def parse(self):
        next_word = Tokenizer.Tokenizer().get_next_token_word()
        if next_word == "(":
            self.alternative = 1
            self.comp = Comparison.Comparison()
            self.comp.parse()
        elif next_word == "!":
            Tokenizer.Tokenizer().skip_token()
            self.alternative = 2
            self.cond1 = Condition.Condition()
            self.cond1.parse()
        elif next_word == "[":
            Tokenizer.Tokenizer().skip_token()
            self.cond1 = Condition.Condition()
            self.cond1.parse()
            if Tokenizer.Tokenizer().get_next_token_word() == "&&":
                self.alternative = 3
                Tokenizer.Tokenizer().skip_token()
                self.cond2 = Condition.Condition()
                self.cond2.parse()
                Tokenizer.Tokenizer().assert_next_token_word("]")
                Tokenizer.Tokenizer().skip_token()
            elif Tokenizer.Tokenizer().get_next_token_word() == "||":
                self.alternative = 4
                Tokenizer.Tokenizer().skip_token()
                self.cond2 = Condition.Condition()
                self.cond2.parse()
                Tokenizer.Tokenizer().assert_next_token_word("]")
                Tokenizer.Tokenizer().skip_token()
            else:
                raise SyntaxError("ERROR: Illegal token \"%s\" discovered, exiting now."
                % (Tokenizer.Tokenizer().get_next_token_word()))
                sys.exit(1)

    def evaluate_cond(self):
        if self.alternative == 1:
            return self.comp.evaluate_comparison()
        if self.s.alternative == 2:
            return not self.cond1.evaluate_cond()
        elif self.s.alternative == 3:
            return self.cond1.evaluate_cond() and self.cond2.evaluate_cond()
        elif self.s.alternative == 4:
            return self.cond1.evaluate_cond() or self.cond2.evaluate_cond()

    def print_cond(self):
        if self.alternative == 1:
            self.comp.print_comp()
        if self.alternative == 2:
            print "!",
            self.cond1.print_cond()
        elif self.alternative == 3:
            print "[",
            self.cond1.print_cond()
            print " && ",
            self.cond2.print_cond()
            print "]",
        elif self.alternative == 4:
            print "[",
            self.cond1.print_cond()
            print " || ",
            self.cond2.print_cond()
            print "]",
