#!/usr/bin/env python
# encoding: utf-8
"""
Loop.py

Created by Maxwell Elliott on 2012-10-23.
Copyright (c) 2012 . All rights reserved.
"""

import Tokenizer
import Condition
import StatementSeq


class Loop:
    """docstring for Loop"""
    def __init__(self):
        self.condition = None
        self.ss = None

    def parse(self):
        Tokenizer.Tokenizer().assert_next_token_word("while")
        Tokenizer.Tokenizer().skip_token()
        self.condition = Condition.Condition()
        self.condition.parse()
        Tokenizer.Tokenizer().assert_next_token_word("loop")
        Tokenizer.Tokenizer().skip_token()
        self.ss = StatementSeq.StatementSeq()
        self.ss.parse()
        Tokenizer.Tokenizer().assert_next_token_word("end")
        Tokenizer.Tokenizer().skip_token()
        Tokenizer.Tokenizer().assert_next_token_word(";")
        Tokenizer.Tokenizer().skip_token()

    def print_loop(self):
        print "while ",
        self.condition.print_cond()
        print " loop "
        self.ss.print_ss()
        print "\tend;"

    def execute(self):
        while self.condition.evaluate_cond():
            self.ss.execute()
