import Tokenizer
import Factor


class Expression:
    def __init__(self):
        self.factor = None
        self.expression = None
        self.alternative = None

    def parse(self):
        self.factor = Factor.Factor()
        self.factor.parse()
        self.alternative = 1
        if Tokenizer.Tokenizer().get_next_token_word() == "+":
            self.alternative = 2
            Tokenizer.Tokenizer().skip_token()
            self.expression = Expression()
            self.expression.parse()
        elif Tokenizer.Tokenizer().get_next_token_word() == "-":
            self.alternative = 3
            Tokenizer.Tokenizer().skip_token()
            self.expression = Expression()
            self.expression.parse()

    def print_exp(self):
        self.factor.print_fac()
        if self.alternative == 2:
            print " + ",
            self.expression.print_exp()
        elif self.alternative == 3:
            print " - ",
            self.expression.print_exp()

    def execute(self):
        if self.alternative == 1:
            return self.factor.execute()
        elif self.alternative == 2:
            return self.factor.execute() + self.expression.execute()
        elif self.alternative == 3:
            return self.factor.execute() - self.expression.execute()
