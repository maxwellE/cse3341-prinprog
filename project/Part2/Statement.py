#!/usr/bin/env python
# encoding: utf-8
"""
Statement.py

Created by Maxwell Elliott on 2012-10-20.
Copyright (c) 2012 . All rights reserved.
"""
import Tokenizer
import sys
import If
import Loop
import Input
import Output
import Assign
import Tab


class Statement:
    """docstring for Statement"""
    def __init__(self):
        self.s = None

    def parse(self):
        next_value = Tokenizer.Tokenizer().get_next_token_value()
        if next_value == 5:
            self.s = If.If()
        elif next_value == 8:
            self.s = Loop.Loop()
        elif next_value == 10:
            self.s = Input.Input()
        elif next_value == 11:
            self.s = Output.Output()
        elif next_value == 32:
            self.s = Assign.Assign()
        else:
            raise SyntaxError("ERROR: Illegal token \"%s\" discovered, exiting now."
                % (Tokenizer.Tokenizer().get_next_token_word()))
            sys.exit(1)
        self.s.parse()

    def print_stmnt(self):
        Tab.Tab.increase_tab()
        Tab.Tab.print_tabs()
        if isinstance(self.s, If.If):
            self.s.print_if()
        elif isinstance(self.s, Loop.Loop):
            self.s.print_loop()
        elif isinstance(self.s, Input.Input):
            self.s.print_input()
        elif isinstance(self.s, Output.Output):
            self.s.print_output()
        elif isinstance(self.s, Assign.Assign):
            self.s.print_assign()
        Tab.Tab.decrease_tab()

    def execute(self):
        self.s.execute()
