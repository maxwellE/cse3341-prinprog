#!/usr/bin/env python
# encoding: utf-8
"""
If.py

Created by Maxwell Elliott on 2012-10-20.
Copyright (c) 2012 . All rights reserved.
"""

import Tokenizer
import Condition
import StatementSeq
import Tab


class If:
    def __init__(self):
        self.condition = None
        self.ss1 = None
        self.ss2 = None

    def parse(self):
        Tokenizer.Tokenizer().assert_next_token_word("if")
        Tokenizer.Tokenizer().skip_token()
        self.condition = Condition.Condition()
        self.condition.parse()
        Tokenizer.Tokenizer().assert_next_token_word("then")
        Tokenizer.Tokenizer().skip_token()
        self.ss1 = StatementSeq.StatementSeq()
        self.ss1.parse()
        print Tokenizer.Tokenizer().get_next_token_word()
        if Tokenizer.Tokenizer().get_next_token_word() == "end":
            Tokenizer.Tokenizer().skip_token()
            Tokenizer.Tokenizer().assert_next_token_word(';')
            Tokenizer.Tokenizer().skip_token()
        else:
            Tokenizer.Tokenizer().assert_next_token_word("else")
            Tokenizer.Tokenizer().skip_token()
            self.ss2 = StatementSeq.StatementSeq()
            self.ss2.parse()
            Tokenizer.Tokenizer().assert_next_token_word("end")
            Tokenizer.Tokenizer().skip_token()
            Tokenizer.Tokenizer().assert_next_token_word(";")
            Tokenizer.Tokenizer().skip_token()

    def print_if(self):
        print "if ",
        self.condition.print_cond()
        print " then "
        self.ss1.print_ss()
        if self.ss2:
            Tab.Tab.print_tabs()
            print "else "
            self.ss2.print_ss()
        Tab.Tab.print_tabs()
        print "end;"

    def execute(self):
        if self.condition.evaluate_cond():
            self.ss1.execute()
        else:
            if self.ss2:
                self.ss2.execute()
