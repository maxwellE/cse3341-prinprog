#!/usr/bin/env python
# encoding: utf-8
import Id
import Tokenizer
import Expression
"""
Assign.py

Created by Maxwell Elliott on 2012-10-23.
Copyright (c) 2012 . All rights reserved.
"""


class Assign:
    def __init__(self):
        self.assign_id = None
        self.expression = None

    def parse(self):
        self.assign_id = Id.Id.ParseID()
        Tokenizer.Tokenizer().assert_next_token_word("=")
        Tokenizer.Tokenizer().skip_token()
        self.expression = Expression.Expression()
        self.expression.parse()
        Tokenizer.Tokenizer().assert_next_token_word(";")
        Tokenizer.Tokenizer().skip_token()

    def print_assign(self):
        self.assign_id.print_id()
        print " = ",
        self.expression.print_exp()
        print ";"

    def execute(self):
        self.assign_id.execute_assign(self.expression.execute())
