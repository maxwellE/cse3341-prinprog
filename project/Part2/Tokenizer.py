#! /usr/bin/env python
import re
import sys


class __Tokenizer:
    instance = None

    def __init__(self, source):
        self.source = source
        self.symbol_dic = {'program': 1, 'begin': 2, 'end': 3,
            'int': 4, 'if': 5,
            'then': 6, 'else': 7, 'while': 8, 'loop': 9,
            'read': 10, 'write': 11, ';': 12, ',': 13,
            '=': 14, '!': 15, '[': 16, ']': 17, '&&': 18,
            '||': 19, '(': 20, ')': 21,
            '+': 22, '-': 23, '*': 24, '!=': 25, '==': 26,
            '<': 27, '>': 28, '<=': 29, '>=': 30}
        self.tokens = self.collect_tokens()
        self.token_index = 0

    def collect_tokens(self):
        operators = ['>=', '<=', '!=', '==', '&&',
            '||', ';', ',', '=', '!', '[',
            ']', '(', ')', '+', '-', '*', '<', '>']
        reserved_words = ["program", "begin", "end", 'int',
            'if', 'then', 'else', 'while', 'loop', 'read', 'write']
        special_tokens = "(" + '|'.join(["%s\\s*" % re.escape(y)
          for y in operators]
          + ["%s\\b\\s*" % x for x in reserved_words]) + ")"
        nums_ids = "(" + "|".join(['\d+\\b\\s*',
          '[A-Z][A-Z0-9]*\\b\\s*']) + ")"
        other_chars = "([^\b\s]+)"
        key_regex = re.compile('|'.join([special_tokens,
          nums_ids, other_chars]))
        copy_source = self.source
        result_tokens = []
        for x in key_regex.finditer(copy_source):
            if x.group(1):
                key = x.group(0).strip()
                result_tokens.append((key, self.symbol_dic[key]))
            elif x.group(2):
                if re.match('\d+\\b\s*', x.group(0)):
                    result_tokens.append((x.group(0).strip(), 31))
                else:
                    result_tokens.append((x.group(0).strip(), 32))
            else:
                if __name__ == "__main__":
                    sys.stderr.write('ERROR: Invalid token \"%s\" detected. Exiting now\n' %
                    x.group(3).strip())
                else:
                    raise SyntaxError(
                    'ERROR: Invalid token \"%s\" detected. Exiting now\n' %
                    x.group(3).strip())
                sys.exit(1)
        result_tokens.append(("EOF", 33))
        return result_tokens

    def get_next_token(self):
        return self.tokens[self.token_index]

    def get_next_token_word(self):
        return self.tokens[self.token_index][0]

    def get_next_token_value(self):
        return self.tokens[self.token_index][1]

    def assert_next_token_value(self, assert_value):
        if(assert_value != self.get_next_token_value()):
            raise SyntaxError("ERROR: Expected a token with value \"%d\" but next token has a value of \"%d\"\n" % (assert_value, self.get_next_token_value()))

    def assert_next_token_word(self, assert_word):
        if(assert_word != self.get_next_token_word()):
            raise SyntaxError("ERROR: Expected a token with the text \"%s\" but next token text is \"%s\"\n" % (assert_word, self.get_next_token_word()))

    def skip_token(self):
        if self.token_index < len(self.tokens):
            self.token_index += 1

    def on_final_token(self):
        return self.token_index == len(self.tokens)


def Tokenizer(source=None):
    if __Tokenizer.instance is None and source is not None:
        __Tokenizer.instance = __Tokenizer(source)
    return __Tokenizer.instance
