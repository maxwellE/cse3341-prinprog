import Declaration
import Tokenizer
"""
DeclarationSeq.py

Created by Maxwell Elliott on 2012-10-17.
"""


class DeclarationSeq:
    """docstring for DeclarationSeq"""
    def __init__(self):
        self.decl = None
        self.ds = None

    def parse(self):
        """docstring for parse"""
        self.decl = Declaration.Declaration()
        self.decl.parse()
        if Tokenizer.Tokenizer().get_next_token_word() != "begin":
            self.ds = DeclarationSeq()
            self.ds.parse()

    def print_ds(self):
        """docstring for print_ds"""
        self.decl.print_decl()
        if self.ds:
            print " ",
            self.ds.print_ds()

    def execute(self):
        """docstring for execute"""
        self.decl.execute()
        if self.ds:
            self.ds.execute()
