import Tokenizer
import IdList


class Output:
    """docstring for Input"""
    def __init__(self):
        self.idlist = None

    def parse(self):
        Tokenizer.Tokenizer().assert_next_token_word("write")
        Tokenizer.Tokenizer().skip_token()
        self.idlist = IdList.IdList()
        self.idlist.parse()
        Tokenizer.Tokenizer().assert_next_token_word(";")
        Tokenizer.Tokenizer().skip_token()

    def print_output(self):
        print "write ",
        self.idlist.print_id_list()
        print ";"

    def execute(self):
        self.idlist.execute_output()
