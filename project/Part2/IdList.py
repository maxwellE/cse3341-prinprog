#!/usr/bin/env python
# encoding: utf-8
import Id
import Tokenizer
"""
IdList.py

Created by Maxwell Elliott on 2012-10-17.
"""


class IdList:
    """docstring for Declaration"""
    def __init__(self):
        self.single_id = None
        self.id_list = None

    def parse(self):
        """docstring for parse"""
        self.single_id = Id.Id.ParseID()
        if Tokenizer.Tokenizer().get_next_token_word() == ",":
            Tokenizer.Tokenizer().skip_token()
            self.id_list = IdList()
            self.id_list.parse()

    def print_id_list(self):
        self.single_id.print_id()
        if self.id_list:
            print ",",
            self.id_list.print_id_list()

    def execute_decl(self):
        self.single_id.execute_decl()
        if self.id_list:
            self.id_list.execute_decl()

    def execute_input(self):
        self.single_id.read_id_val()
        if self.id_list:
            self.id_list.execute_input()

    def execute_output(self):
        self.single_id.print_id_val()
        if self.id_list:
            self.id_list.execute_output()
