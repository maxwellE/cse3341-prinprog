#!/usr/bin/env python
# encoding: utf-8
"""
Id.py

Created by Maxwell Elliott on 2012-10-18.
"""
import Tokenizer
import sys
import Input


class Id:
    id_list = []
    """docstring for Id"""
    def __init__(self, name):
        self.name = name
        self.val = None
        self.declared = None
        self.initialized = None

    @staticmethod
    def ParseID():
        if Id.id_exists(Tokenizer.Tokenizer().get_next_token_word()) is True:
            ret_id = Id.get_id(Tokenizer.Tokenizer().get_next_token_word())
            Tokenizer.Tokenizer().skip_token()
            return ret_id
        else:
            new_id = Id(Tokenizer.Tokenizer().get_next_token_word())
            Tokenizer.Tokenizer().skip_token()
            Id.id_list.append(new_id)
            return new_id

    def print_id(self):
        print self.name,

    def id_name(self):
        return self.name

    def print_id_val(self):
        print "%s = %d" % (self.name, self.val)

    def execute_decl(self):
        Id.dec_id(self.name)

    def read_id_val(self):
        if Id.is_dec(self.name) and Input.Input.has_value():
            self.val = Input.Input.get_next_input_value()
        else:
            raise SyntaxError("RUNTIME ERROR: You are attempting to read the value from an undeclared or uninitialzed id (\"%s\"), exiting now."
            % (self.name))
            sys.exit(1)

    def execute_op(self):
        if Id.is_dec(self.name):
            return Id.get_id(self.name).val
        else:
            raise SyntaxError("RUNTIME ERROR: You are attempting to read the value from an undeclared or uninitialzed id (\"%s\"), exiting now."
            % (self.name))
            sys.exit(1)

    def execute_assign(self, val):
        if Id.is_dec(self.name):
            Id.assign_val(self.name, val)
        else:
            raise SyntaxError("RUNTIME ERROR: You are attempting to read the value from an undeclared or uninitialzed id (\"%s\"), exiting now."
            % (self.name))
            sys.exit(1)

    @staticmethod
    def id_exists(name):
        for iterid in Id.id_list:
            if iterid.name == name:
                return True
        return False

    @staticmethod
    def get_id(name):
        for iter_id in Id.id_list:
            if iter_id.name == name:
                return iter_id

    @staticmethod
    def dec_id(name):
        for iterid in Id.id_list:
            if iterid.name == name:
                iterid.declared = True

    @staticmethod
    def is_dec(name):
        for iterid in Id.id_list:
            if iterid.name == name:
                return iterid.declared

    @staticmethod
    def is_init(name):
        for iterid in Id.id_list:
            if iterid.name == name:
                return iterid.initialized

    @staticmethod
    def assign_val(name, val):
        for iterid in Id.id_list:
            if iterid.name == name:
                iterid.val = val
