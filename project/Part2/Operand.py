import Tokenizer
import Expression
import Id
import sys


class Operand:
    def __init__(self):
        self.integer = None
        self.op_id = None
        self.expression = None

    def parse(self):
        if Tokenizer.Tokenizer().get_next_token_word() == "(":
            Tokenizer.Tokenizer().skip_token()
            self.expression = Expression.Expression()
            self.expression.parse()
            Tokenizer.Tokenizer().assert_next_token_word(")")
            Tokenizer.Tokenizer().skip_token()
        else:
            if Tokenizer.Tokenizer().get_next_token_value() == 31:
                self.integer = Tokenizer.Tokenizer().get_next_token_word()
                Tokenizer.Tokenizer().skip_token()
            elif Tokenizer.Tokenizer().get_next_token_value() == 32:
                self.op_id = Id.Id.ParseID()
            else:
                raise SyntaxError("ERROR: Illegal token \"%s\" discovered, exiting now."
                % (Tokenizer.Tokenizer().get_next_token_word()))
                sys.exit(1)

    def print_op(self):
        if self.integer:
            print self.integer,
        elif self.op_id:
            self.op_id.print_id()
        elif self.expression:
            print "( "
            self.expression.print_exp()
            print " )"

    def execute(self):
        if self.integer:
            return int(self.integer)
        elif self.op_id:
            return self.op_id.execute_op()
        elif self.expression:
            return self.expression.execute()
